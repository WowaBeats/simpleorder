var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);


io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('update_order', function(msg){
        console.log('updates orders')
        io.emit('update_order', {for: 'everyone'})
    });

    socket.on('update_dish', function(msg){
        console.log('updates dish')
        io.emit('update_dish', {for: 'everyone'})
    });

    socket.on('update_person', function(msg){
        console.log('updates person')
        io.emit('update_person', {for: 'everyone'})
    });

});

http.listen(3000, function(){
    console.log('listening on *:3000');
});