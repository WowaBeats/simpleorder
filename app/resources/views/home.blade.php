<!doctype html>
<html lang="en">
<head>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet">

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>simpleorder</title>

</head>
<body>

<div id="app">
    <v-app>
        <Navigation></Navigation>
        <router-view style="margin-bottom:100px;"></router-view>
    </v-app>
</div>

<script src="{{asset('js/main.js')}}"></script>

</body>
</html>