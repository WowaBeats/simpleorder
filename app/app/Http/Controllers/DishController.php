<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dish;

class DishController extends Controller
{
    public function store(Request $request){
        $reqData = $request->validate([
            'title' => 'string|required',
            'description' => 'string|required'
        ]);

        $dish = Dish::create([
            'title' => $reqData["title"],
            'description' => $reqData["description"]
        ]);

        return $dish->toJson();
    }

    public function destroy(Request $request, Dish $dish){
        $dish->delete();

        return json_encode(array('message' => 'Dish deleted successfully.'));
    }

    public function update(Request $request, Order $order){
        $reqData = $request->validate([
            'count' => 'integer',
            'status' => 'required|string'
        ]);

        $order->status = $reqData["status"];

        if($request->has('count')){
            $order->count = $reqData["count"];
        }

        $order->save();

        return $order->toJson();
    }
}
