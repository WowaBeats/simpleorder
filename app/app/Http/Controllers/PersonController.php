<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;

class PersonController extends Controller
{
    public function store(Request $request){
        $reqData = $request->validate([
            'name' => 'string|required'
        ]);

        $person = Person::create([
            'name' => $reqData["name"]
        ]);

        return $person->toJson();

    }

    public function destroy(Request $request, Person $person){
        $person->delete();

        return json_encode(array('message' => 'Person deleted successfully.'));
    }
}
