<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    public function store(Request $request){
        $reqData = $request->validate([
            'person_id' => 'required|exists:persons,id',
            'dish_id' => 'required|exists:dishes,id',
            'count' => 'integer',
            'status' => 'string'
        ]);

        $order = Order::create([
            'person_id' => $reqData["person_id"],
            'dish_id' => $reqData["dish_id"]
        ]);

        if($request->has('count')){
            $order->count = $reqData["count"];
        }

        if($request->has('status')){
            $order->status = $reqData["status"];
        }

        $order->save();

        return $order->toJson();
    }

    public function destroy(Request $request, Order $order){
        $order->delete();

        return json_encode(array('message' => 'Order deleted successfully.'));
    }

    public function update(Request $request, Order $order){
        $reqData = $request->validate([
            'status' => 'required|string',
            'count' => 'integer'
        ]);

        $order->status = $reqData["status"];

        if($request->has('count')){
            $order->count = $reqData["count"];
        }

        $order->save();

        return $order->toJson();
    }
}
