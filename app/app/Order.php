<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Person;
use App\Dish;

class Order extends Model
{
    protected $fillable = ['dish_id','person_id','count','status'];

    public function person(){
        return $this->belongsTo(Person::class);
    }

    public function dish(){
        return $this->belongsTo(Dish::class);
    }
}
