<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Dish extends Model
{
    protected $fillable = ['title','description'];

    public function orders(){
        return $this->hasMany(Order::class);
    }
}
