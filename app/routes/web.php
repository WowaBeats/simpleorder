<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Person;
use App\Dish;
use App\Order;

Route::get('/', function () {
    return view('home');
});

Route::get('/persons',function(){
   return new \App\Http\Resources\PersonCollection(Person::all());
});

Route::get('/dishes',function(){
    return new \App\Http\Resources\DishCollection(Dish::all());
});

Route::get('/orders',function(){
    return new \App\Http\Resources\OrderCollection(Order::with(['dish','person'])->get());
});

Route::get('/person/{person}/orders',function(Request $request, Person $person){
    return new OrderCollection($person->orders->with('dishes'));
});

/**
 * POST
 * */

//Add new person
Route::post('/person','PersonController@store');

//Add new dish
Route::post('/dish','DishController@store');

//Add new Order
Route::post('/order','OrderController@store');

/**
 * DELETE
 * */

//Delete person
Route::delete('/person/{person}','PersonController@destroy');

//Delete dish
Route::delete('/dish/{dish}','DishController@destroy');

//Delete order
Route::delete('/order/{order}','OrderController@destroy');

/**
 * PUT
 * */

//Update order
Route::put('/order/{order}','OrderController@update');